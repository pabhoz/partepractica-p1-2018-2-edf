##Parte Práctica

Escriba dos elementos propios que funcionen como botones, uno tendrá la clase blop y el otro la clase snap
cada botón debe tener un comportamiento específico que se describirá a continuación:

### Botón Blop

1. El botón blop reacciona a los click incrementando mediante una animación que simula el caer de una gota
ampliandose (10% de su tamaño asigando) y luego volviendo a su tamaño original, haciendo esto 3 veces, pero cada
vez consecutiva su tamaño se ampliará sólo la mitad, lo que nos lleva a que un click hará que el botón se amplie
y regrese a su tamaño 3 veces antes de terminar la animación correspondiente a dicho click, esto con una duración
total de 2 segundos
2. El botón reproducirá el sonido blop.mp3 al ser presionado
3. Si el botón es presionado nuevamente, volverá a reproducir el sonido desde el inicio aunque aún este reproduciendo

### Botón snap

1. El botón snap funciona como un microorganismo y cada vez que sea presionado creará una copia de el por la cantidad
total de veces presionado, así que al primer click, creará 1 copia, al segundo dos y así sucesivamente. Cada nueva copia
inicia su contador de clicks en cero.
2. El botón reproducirá el sonido snap.mp3 al ser presionado
3. Si el botón es presionado nuevamente, volverá a reproducir el sonido desde el inicio aunque aún este reproduciendo