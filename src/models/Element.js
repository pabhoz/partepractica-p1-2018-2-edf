class Element{

    constructor(){
        this._DOMElement = null;
    }
  
    get DOMElement(){
      return this._DOMElement;
    }
  
    set DOMElement(value){
      this._DOMElement = value;
    }
  
    initDOMElement(){

    }
  
    dataBind(e,variable){
      this[variable] = e.target.value;
      console.log(`${variable} => ${this[variable]}`);
    }
  
  }